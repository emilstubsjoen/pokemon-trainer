import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CataloguePage } from './pages/catalogue.page';
import { CatalogueRoutingModule } from './catalogue-routing.module'; 
import { CatalogueComponent } from './components/catalogue/catalogue.component'; 
import { CatalogueItemComponent } from './components/catalogue-item/catalogue-item.component'; 

@NgModule({
  declarations: [
    // Pages
    CataloguePage,
    // Components
    CatalogueComponent,
    CatalogueItemComponent,
  ],
  imports: [
    SharedModule,
    CatalogueRoutingModule
  ]
})
export class CatalogueModule {
}
