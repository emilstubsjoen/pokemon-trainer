import { Component } from "@angular/core";
import { AppRoutes } from "src/app/shared/enums/app-routes.enum";
import { Router } from "@angular/router";

@Component({
    selector: 'app-catalogue-page',
    templateUrl: './catalogue.page.html',
    styleUrls: ['./catalogue.page.css']
})
export class CataloguePage {
    constructor( private readonly router: Router) {
        // Redirect to login if user not in in local storage
        if (!localStorage.getItem('session')) {
            this.router.navigate([AppRoutes.Login]);
        }
    }
}



