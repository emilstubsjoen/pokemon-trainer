import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Pokemon } from "src/app/shared/models/pokemon.model"; 

@Component({
    selector: 'app-catalogue-item',
    templateUrl: './catalogue-item.component.html',
    styleUrls: ['./catalogue-item.component.css']
})
export class CatalogueItemComponent {
    @Input() pokemon: Pokemon | undefined;
    @Output() caught: EventEmitter<Pokemon> = new EventEmitter()

    public onPokemonCaught(): void {
        // Notify the parent the item was clicked
        this.caught.emit(this.pokemon)
    }
    
}