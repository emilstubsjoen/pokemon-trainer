import { Component, OnInit } from "@angular/core";
import { Pokemon } from "src/app/shared/models/pokemon.model"; 
import { CaughtPokemonService } from "../../../services/caught-pokemon.service";
import { PokemonCatalogueService } from "../../../services/pokemon-catalogue.service";

@Component({
    selector: 'app-catalogue',
    templateUrl: './catalogue.component.html',
    styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements  OnInit{
    constructor( 
        private readonly pokemonCatalogueService: PokemonCatalogueService,
        private readonly caughtPokemonService: CaughtPokemonService 
        ) {
    }
    
    ngOnInit(): void {
        this.pokemonCatalogueService.fetchPokemonData();
    }

    get pokemons(): Pokemon[] {
        return this.pokemonCatalogueService.getPokemons()
    }

    public handlePokemonCaught(pokemon: Pokemon): void {
        if (!pokemon.caught) {
            pokemon.caught = true
            this.caughtPokemonService.addPokemon(pokemon)
            console.log('adding pokemon', pokemon)
        }else {
            console.log('removing pokemon', pokemon)
            pokemon.caught = false
            this.caughtPokemonService.removePokemon(pokemon)
        }
    }

}