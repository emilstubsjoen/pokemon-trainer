import {Component} from '@angular/core';

@Component({
  selector: 'app-container',
  template: `
    <div class="container">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
    `.container {
      max-width: 100em;
      width: 100%;
      margin: 0 auto;
      padding: 0 1em;
    }`
  ]
})
export class ContainerComponent {
}
