export interface PokemonData {
    count: number,
    next: string,
    previous: object
    results: Pokemon[]
    }

export interface Pokemon {
    id: number;
    name: string;
    url: string;
    imgUrl: string;
    caught: boolean;
}