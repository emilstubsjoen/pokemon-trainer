import { Injectable } from '@angular/core';
import { AppRoutes } from '../shared/enums/app-routes.enum'; 
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private readonly router: Router) {}

  public login(username: string) {
    localStorage.setItem('session', JSON.stringify(username))
    this.router.navigate([AppRoutes.Catalogue]);
    console.log('logged in.')
    
    
  }

}
