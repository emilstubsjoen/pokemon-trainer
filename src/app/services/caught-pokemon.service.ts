import { Injectable } from "@angular/core";
import { Pokemon } from "../shared/models/pokemon.model"; 

@Injectable({
    providedIn: 'root'
})

export class CaughtPokemonService {
    private caughtPokemons: Pokemon[]

    constructor() {
      this.caughtPokemons = []
      const cache = localStorage.getItem('caught-pokemons')
      if (cache) {
        this.caughtPokemons = JSON.parse(cache)
      }
    }
  
    addPokemon(pokemon: Pokemon) {
      if (!this.caughtPokemons.some(caughtPokemon => caughtPokemon.name === pokemon.name)){
        this.caughtPokemons.push(pokemon)
        this.cachePokemon() 
      }
      
    }

    removePokemon(pokemon: Pokemon) {
      this.caughtPokemons = this.caughtPokemons.filter((caughtPokemon, index) => caughtPokemon.name !== pokemon.name)
      this.cachePokemon() 
    }
  
    cachePokemon() {
      console.log(this.caughtPokemons)
      localStorage.setItem('caught-pokemons', JSON.stringify(this.caughtPokemons))
      console.log('pokemon added to local storage.', this.caughtPokemons)
    }
    
  
    getPokemons(): Pokemon[] {
      // no need to read from storage. It's already done in the constructor.
      return this.caughtPokemons;
    }
  }