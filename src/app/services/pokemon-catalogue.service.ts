import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PokemonData } from '../shared/models/pokemon.model'; 
import { Pokemon } from '../shared/models/pokemon.model'; 
import { Router } from '@angular/router';
import { AppRoutes } from '../shared/enums/app-routes.enum';

@Injectable({
  providedIn: 'root',
})
export class PokemonCatalogueService {
  private pokemons: Pokemon[] = []
  private error: string = '';

  constructor(private readonly http: HttpClient) {}

  public fetchPokemonData(): void {
    this.http
      .get<PokemonData>('https://pokeapi.co/api/v2/pokemon')
      .subscribe((pokemonData: PokemonData) => {
        this.pokemons = pokemonData.results
        .map((pokemon, index) => ({
          id: index+1,
          name: pokemon.name.charAt(0).toUpperCase() + pokemon.name.slice(1), 
          url: pokemon.url,
          imgUrl: `https://raw.githubusercontent.com/PokeAPI/sprites/84204f8594790cfd04190a8d82beb31f49115c02/sprites/pokemon/other/dream-world/${index+1}.svg`,
          caught: false
        }));  
      }),
      (error: HttpErrorResponse) => {
        this.error = error.message;
      };
  }

  public getPokemons(): Pokemon[] {
    this.update()
    return this.pokemons
  }

  private update(){
    const cache = localStorage.getItem('caught-pokemons')
      if (cache) {
        const caughtPokemons = JSON.parse(cache)
        for (let i = 0; i < this.pokemons.length; i++){
          for (let j = 0; j < caughtPokemons.length; j++){
            if (this.pokemons[i].name === caughtPokemons[j].name){
              this.pokemons[i].caught = true
            }
          }
        }
      }
  }
}

