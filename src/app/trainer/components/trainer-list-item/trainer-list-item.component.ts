import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Pokemon } from "src/app/shared/models/pokemon.model"; 

@Component({
    selector: 'app-trainer-list-item',
    templateUrl: './trainer-list-item.component.html',
    styleUrls: ['./trainer-list-item.component.css']
})
export class TrainerListItemComponent {
    @Input() pokemon: Pokemon | undefined;
    @Output() caught: EventEmitter<Pokemon> = new EventEmitter()

    
    public onPokemonRemove(): void {
        // Notify the parent the item was clicked
        //if (this.pokemon) this.pokemon.caught = !this.pokemon.caught
        //this.caught.emit(this.pokemon)
    }
    
}