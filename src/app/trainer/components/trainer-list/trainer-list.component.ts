import { Component, OnInit } from "@angular/core";
import { Pokemon } from "src/app/shared/models/pokemon.model"; 
import { CaughtPokemonService } from "../../../services/caught-pokemon.service";

@Component({
    selector: 'app-trainer-list',
    templateUrl: './trainer-list.component.html',
    styleUrls: ['./trainer-list.component.css']
})
export class TrainerListComponent implements  OnInit{
    constructor( 
        private readonly caughtPokemonService: CaughtPokemonService 
        ) {
    }
    
    ngOnInit(): void {
    }

    get pokemons(): Pokemon[] {
        return this.caughtPokemonService.getPokemons()

    }

}