import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TrainerPage } from './pages/trainer.page'; 
import { TrainerRoutingModule } from './trainer-routing.module'; 
import { TrainerListComponent } from './components/trainer-list/trainer-list.component'; 
import { TrainerListItemComponent } from './components/trainer-list-item/trainer-list-item.component';

@NgModule({
  declarations: [
    // Pages
    TrainerPage,
    // Components
    TrainerListComponent,
    TrainerListItemComponent

  ],
  imports: [
    SharedModule,
    TrainerRoutingModule
  ]
})
export class TrainerModule {
}
