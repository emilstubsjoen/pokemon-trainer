import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AppRoutes } from "src/app/shared/enums/app-routes.enum";

@Component({
    selector: 'app-trainer',
    templateUrl: './trainer.page.html',
    styleUrls: ['./trainer.page.css']
})
export class TrainerPage {
    constructor( private readonly router: Router) {
        // Redirect to login if user not in in local storage
        if (!localStorage.getItem('session')) {
            this.router.navigate([AppRoutes.Login]);
        }
    }
}

