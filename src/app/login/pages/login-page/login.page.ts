import {Component} from '@angular/core';
import {User} from '../../../shared/models/user.model';
import {AppRoutes} from '../../../shared/enums/app-routes.enum';
import {Router} from '@angular/router';
import { LoginService } from 'src/app/services/loginService';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html'
})
export class LoginPage {

  constructor(private readonly router: Router,
              private readonly loginService: LoginService 
  ) {
      // Redirect to catalogue if user is in local storage
      if (localStorage.getItem('session')) {
        this.router.navigate([AppRoutes.Catalogue]);
      }
    }

  handleLoginSuccess(username: string): Promise<boolean> {
    this.loginService.login(username)
    return this.router.navigate([AppRoutes.Catalogue]);
  }

}
