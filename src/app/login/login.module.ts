import {NgModule} from '@angular/core';
import {LoginPage} from './pages/login-page/login.page';
import {LoginRoutingModule} from './login-routing.module';
import {LoginFormComponent} from './components/login-form/login-form.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    LoginPage,
    LoginFormComponent
  ],
  imports: [
    SharedModule,
    FormsModule,
    LoginRoutingModule
  ]
})
export class LoginModule {
}
