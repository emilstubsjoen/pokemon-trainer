import {Component, EventEmitter, Output} from '@angular/core';
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html'
})
export class LoginFormComponent {
  @Output() login: EventEmitter<string> = new EventEmitter()

  // Properties
  public username: string

  constructor() {
    this.username = ''

  }
  onLoginClick() {
    this.login.emit(this.username)

  }

}
